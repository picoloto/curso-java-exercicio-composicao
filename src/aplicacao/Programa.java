package aplicacao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import entidades.Contrato;
import entidades.Departamento;
import entidades.Trabalhador;
import entidades.enums.NivelExperiencia;

public class Programa {

	public static void main(String[] args) throws ParseException {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		System.out.print("Nome do Departamento: ");
		String departamento = sc.nextLine();

		System.out.println("Dados do Trabalhador");
		System.out.print("Nome: ");
		String nome = sc.nextLine();
		System.out.print("N�vel: ");
		String nivel = sc.nextLine();
		System.out.print("Salario Base: ");
		double salarioBase = sc.nextDouble();

		Trabalhador trabalhador = new Trabalhador(nome, NivelExperiencia.valueOf(nivel), salarioBase,
				new Departamento(departamento));
		
		System.out.println();
		System.out.print("Quantos contratos o trabalhador possui? ");
		int qtdContratos = sc.nextInt();
		
		for (int i = 1; i <= qtdContratos; i++) {
			System.out.println("Dados do contrato " + i + ":");
			System.out.print("Data (DD/MM/YYYY): ");
			Date dataContrato = sdf.parse(sc.next());
			System.out.print("Valor por hora(00.00): ");
			double valorPorHora = sc.nextDouble();
			System.out.print("Dura��o (horas): ");
			int horas = sc.nextInt();
			
			Contrato contrato = new Contrato(dataContrato, valorPorHora, horas);
			trabalhador.addContrato(contrato);
			System.out.println();
		}
		
		System.out.println();
		
		System.out.print("Informe o periodo (MM/YYYY) para calcular o salario ganho: ");
		String mesAno = sc.next();
		int mes = Integer.parseInt(mesAno.substring(0, 2));
		int ano = Integer.parseInt(mesAno.substring(3));
		
		System.out.println("Nome: " + trabalhador.getName());
		System.out.println("Departamento: " + trabalhador.getDepartamento().getName());
		System.out.println("Ganho referente ao periodo " 
		+ mesAno 
		+ ": " 
		+ String.format("%.2f", trabalhador.ganhoMensal(ano, mes)));

		sc.close();

	}

}
