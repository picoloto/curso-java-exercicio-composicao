package entidades;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import entidades.enums.NivelExperiencia;

public class Trabalhador {

	private String name;
	private NivelExperiencia nivel;
	private Double salarioBase;

	private Departamento departamento;
	private List<Contrato> contratos = new ArrayList<>();

	public Trabalhador() {
	}

	public Trabalhador(String name, NivelExperiencia nivel, Double salarioBase, Departamento departamento) {
		this.name = name;
		this.nivel = nivel;
		this.salarioBase = salarioBase;
		this.departamento = departamento;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public NivelExperiencia getNivel() {
		return nivel;
	}

	public void setNivel(NivelExperiencia nivel) {
		this.nivel = nivel;
	}

	public Double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(Double salarioBase) {
		this.salarioBase = salarioBase;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public List<Contrato> getContratos() {
		return contratos;
	}

	public void addContrato(Contrato contrato) {
		contratos.add(contrato);
	}

	public void removeContrato(Contrato contrato) {
		contratos.remove(contrato);
	}

	public double ganhoMensal(int ano, int mes) {
		double soma = salarioBase;
		Calendar cal = Calendar.getInstance();

		for (Contrato c : contratos) {
			cal.setTime(c.getDataContrato());
			int c_ano = cal.get(Calendar.YEAR);
			int c_mes = 1 + cal.get(Calendar.MONTH);
						
			if (ano == c_ano && mes == c_mes) {
				soma += c.valorTotal();
			}
		}
		return soma;
	}

}
