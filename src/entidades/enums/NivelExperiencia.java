package entidades.enums;

public enum NivelExperiencia {
	
	JUNIOR,
	PLENO,
	SENIOR;
}
